# Noodle, a discord bot

Noodle comes in three parts

## run-noodle-online.py
This is the bot code that interacts with Discord

## noodle.py
This is going to be the bulk of the code. Working with requests
whether from Discord or elsewhere.

## test-noodle.py
This is going to be a command-line tool for testing noodle
without Discord.

# Features
To be determined.

import noodle
import asyncio

current_channel = "fake"

class FakeChannel:
    name = ""
    def __init__(self):
        self.name = current_channel

class FakeMessage:
    channel = FakeChannel() 
    author = "me"
    content = ""
    def __init__(self, message):
        self.channel = FakeChannel()
        self.content = message

async def myTester():
    global current_channel
    current_channel = "fake_bot_spam"

    while True:
        text = input("noodle> ")
        if text == "quit":
            break
        fm = FakeMessage(text)
        noodle.command(fm)

asyncio.run(myTester())

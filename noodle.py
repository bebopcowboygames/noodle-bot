# Commands
first = True
count = 0
send_message = ""

def command(msg):
    global count
    count += 1

    # if count < 1:
    #   global send_message
    #   send_message = "test"

    printMessageInfo(msg)
    
def printMessageInfo(msg):
    print(f"#{count}\n"\
      + f"Posted in {msg.channel.name}\n"\
      + f"By user: {msg.author}\n"\
      + f"Saying: {msg.content}")


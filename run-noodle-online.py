import discord
import config
import noodle

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.lower().startswith('noodle'):
        noodle.command(message)

    global send_message
    if noodle.send_message !="":
        await message.channel.send(noodle.send_message)
        noodle.send_message = ""

# KEY is discord bot token
client.run(config.KEY)
